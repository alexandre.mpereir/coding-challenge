#ifndef CACHE_CLASS_H
#define CACHE_CLASS_H



using namespace std;
class Cache{

protected:
    string element;
    string timestamp;
public:
    void SetElement(string element){this->element = element;}
    string GetElement(){return element;}
    void SetTimestamp(string timestamp){this->timestamp = timestamp;}
    string GetTimestamp(){return timestamp;}
    void add(string);
};

void Cache::add(string element){
    this->element=element;
    time_t curr_time;
    curr_time = time(NULL);
    char *tm = ctime(&curr_time);
    this->timestamp = tm;
}


#endif
