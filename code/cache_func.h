#ifndef CACHE_FUNC_H
#define CACHE_FUNC_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <stdio.h>
#include <string.h>
#include <ctime>
#include <sstream>
#include "cache_class.h"


using namespace std;
string str_part_0= "cache.";
string str_part_1= "add(";
string str_part_2= "print(";
string str_part_3=")";
string str_part_4= "edit(";
string str_part_5= "cache(";
string str_part_6= "Cache";
int counter=0;
int CacheSize(){
    int size,flag=0,flag2=0;string arg1; char arg2; int arg3;
    while(flag!=2)
    {
        flag=0;
        do{
            cin >> arg1 >> arg2 >> arg3;
            if(arg2!='=' && !cin){
                cout << "Not the intended format\n";
                cin.clear();
                cin.ignore(100, '\n');
                continue;
            }
            else flag++;
            if(flag!=1) cout << "\nBad Input, Please re-enter ex:size = 3\n";
        }while(flag!=1);
        if(arg3<=0) cout << "\n Please enter a value bigger than zero";
        else flag++;
    }
    flag=0;
    string arg4,arg5;
    while(flag!=4){
        flag=0;
        cin >> arg4 >> arg5;
        if(str_part_6.compare(0,5,arg4,0,5) == 0) {
            flag++;
        }
        if(str_part_5.compare(0,6,arg5,0,6) == 0) {
            arg5.erase(0,6);
            flag++;
        }
        string last_char(1,arg5.back());
        if(str_part_3.compare(0,1,last_char,0,1) == 0) {
            flag++;
            arg5.pop_back();
        }
        int size_arg1=arg1.size();
        if(arg1.compare(0,size_arg1,arg5,0,size_arg1) == 0){
            flag++;
        }
        if(flag!=4) cout << "Not the intended format ex: Cache cache(size)\n";
        
    }

    size=arg3;
    return size;
}
void ErrorText(string str_submitted){
    cout << " '"<< str_submitted << "' is not recognized as an command\n\n";
    cout << "Command suggestions:" <<"\n";
    cout <<"cache.add(x)   - adds the value x to the cache;" << "\n";
    cout <<"cache.print()  - prints all available elements present inside of the cache;" << "\n";
    cout <<"cache.edit(x)  - edits the value in the x position inside of the cache; "<< "\n";
}

string GetCacheElement(){
    string str_submitted;
    while(str_part_0.compare(0,6,str_submitted,0,6) != 0) {
        cin >> str_submitted ;
        if(str_part_0.compare(0,6,str_submitted,0,6) !=0)ErrorText(str_submitted);
    }
    str_submitted.erase(0,6);
    string last_cha(1,str_submitted.back());
    if (str_part_3.compare(0,1,last_cha,0,1) == 0) str_submitted.pop_back();
    return str_submitted;
}
void AddToCache(string str_submitted,int cache_size, Cache cache[]){
        str_submitted.erase(0,4);

        if(counter < cache_size) {
            cache[counter].add(str_submitted);
            counter++;
        }
        else{
            for (int i = 0; i < cache_size-1; i++){
                cache[i].SetElement(cache[i+1].GetElement());
                cache[i].SetTimestamp(cache[i+1].GetTimestamp());
            }
            cache[cache_size-1].add(str_submitted);
                    
        }
        cout<<"> ok \n";
}
void PrintCache(string str_submitted,int cache_size, Cache cache[]){
        
        str_submitted.erase(0,6);
        if(str_submitted.empty()){
            cout<<"\n>Cache \n  > ";
            for(int a=0; a < cache_size;a++) cout << cache[a].GetElement() << "; ";
            cout<<"\n>Timestamp: \n ";
            for(int a=0; a < cache_size;a++) {       
                cout << "  " << a+1 << " : "<< cache[a].GetTimestamp() << " ";
            }    
            cout << "\n"; 
        }
        else{
            str_submitted="cache.print("+str_submitted+")";
            ErrorText(str_submitted);
        }
        
        
}
void EditCache(string aux_string, int place,int cache_size, Cache cache[]){
    place--;
    for(int i = place; i < cache_size-1;i++){
            cache[i].SetElement(cache[i+1].GetElement());
            cache[i].SetTimestamp(cache[i+1].GetTimestamp());

    }
    cache[cache_size-1].add(aux_string);
}
void UpdateCache(string str_submitted,int cache_size,Cache cache[]){
    int place=0;
    str_submitted.erase(0,5);
    stringstream aux_word(str_submitted);
    aux_word >> place;
    if(place<=0|| place>cache_size) cout << "Please submit a value between 1 and " << cache_size << "\n";
    else{ 
        string aux_string;
        int aux_place=place;
        cout << "what value do you want to submit at " << aux_place;
        if(aux_place==1)cout <<"st";
        if(aux_place==2)cout <<"nd";
        if(aux_place==3)cout <<"rd";
        if(aux_place>3)cout <<"th";
        cout << " place of the cache?\n";
        cin >> aux_string;
        EditCache(aux_string,place,cache_size,cache);
    }
}

#endif

